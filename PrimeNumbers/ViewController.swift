//
//  ViewController.swift
//  PrimeNumbers
//
//  Created by Joshua on 16/1/19.
//  Copyright © 2019 Joshua. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var firstNumberTextField: UITextField!
    @IBOutlet weak var secondNumberTextField: UITextField!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    let bag = DisposeBag()
    
    enum MyError: Error {
        case AnError
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        [firstNumberTextField, secondNumberTextField].forEach({ $0.addTarget(self, action: #selector(checkStartButton), for: .editingChanged) })
        firstNumberTextField.text = "1"
        secondNumberTextField.text = "1000"
        startButton.isUserInteractionEnabled = true
        startButton.alpha = 1.0
    }


    
    func findPrimesEratosthenes(_ first: Int, _ second: Int) -> (String, Int) {
        
        if (first > second) {
            return ("", -1)
        }
        
        //    print("Hi");
        var resultString: String = "";
        var boolArr = Array(repeating: true, count: second);
        boolArr[0] = false; // 1 is not a prime
        var terminalInt = Int(sqrt(Double(second)));
        
        if (terminalInt < 2) {
            terminalInt = second
        }
        
        
        for i in 1...terminalInt {
            //        print("i ", i);
            if (boolArr[i-1] == true) {
                //Number i has not been crossed out yet
                for j in stride(from: i*i, through: second, by: i) {
                    //                print("j ", j);
                    boolArr[j-1] = false
                }
            }
            
        }
        var count = 0;
        for i in first...second {
            //        print("i ", i)
            if (boolArr[i-1] == true) {
                // Found prime
                // Return observable
                
                DispatchQueue.main.async {
                    self.resultLabel.text = "\(i)"
                    self.countLabel.text = "Count = \(count)"
                }
                
                count += 1;
                
                if (resultString == "") {
                    resultString += "\(i)";
                } else {
                    resultString += " \(i)";
                }
                print(i);
            }
        }
        return (resultString, count)
    }
    
    func createObservableForPrimes() -> Observable<Int> {
        return Observable<Int>.create { observer in
            return Disposables.create()
        }
    }
    
    func createPublishSubject() -> PublishSubject<Int> {
        return PublishSubject<Int>()
    }
    
    
    func findPrimesEratosthenesRX(_ first: Int, _ second: Int) -> Observable<(Int, Int)> {
        return Observable<(Int, Int)>.create{ emitter in
            if (first > second) {
                emitter.on(.error(MyError.AnError))
                return Disposables.create()
            }
            
            var keepRunning = true
            var resultString: String = "";
            var boolArr = Array(repeating: true, count: second);
            boolArr[0] = false; // 1 is not a prime
            var terminalInt = Int(sqrt(Double(second)));
            
            if (terminalInt < 2) {
                terminalInt = second
            }
            
            
            for i in 1...terminalInt {
                //        print("i ", i);
                if (boolArr[i-1] == true) {
                    //Number i has not been crossed out yet
                    for j in stride(from: i*i, through: second, by: i) {
                        //                print("j ", j);
                        boolArr[j-1] = false
                    }
                }
                
            }
            var count = 0;
            DispatchQueue.global().async(execute: {
                for i in first...second {
                    if (!keepRunning) {break}
                    //        print("i ", i)
                    if (boolArr[i-1] == true) {
                        // Found prime, emit
                        count += 1;
//                        print("Submitting: \(i)")
                        emitter.on(.next((i, count)))
                    }
                }
            })
            return Disposables.create {
                print("Observable was disposed.")
                keepRunning = false
            }
        }
        
    }
    
    @IBAction func onStartButton(_ sender: Any) {
        self.view.endEditing(true);
        resultLabel.text = ""
        countLabel.text = ""
        let first = Int(firstNumberTextField.text!)!
        let second = Int(secondNumberTextField.text!)!
        
        DispatchQueue.global(qos: .userInitiated).async {
            let resToDisplay = self.findPrimesEratosthenes(first, second)
            DispatchQueue.main.async {
                if (resToDisplay.1 == -1) {
                    self.resultLabel.textColor = UIColor.red
                    self.resultLabel.text = "Error: First Number is greater than second number"
                } else {
                    if (resToDisplay.0.isEmpty) {
                        self.resultLabel.textColor = UIColor.red
                        self.resultLabel.text = "There are no prime numbers in this range"
                    } else {
                        self.resultLabel.textColor = UIColor.black
//                        self.resultLabel.text = resToDisplay.0
                    }
                }
                self.countLabel.text = "Count =\(resToDisplay.1)"

            }
        }
        
//        self.resultLabel.textColor = UIColor.blue
//        self.resultLabel.text = "Processing..."
        
    }
    
    
    @IBAction func onStartButtonRX(_ sender: Any) {
        self.view.endEditing(true);
        resultLabel.text = ""
        countLabel.text = ""
        let first = Int(firstNumberTextField.text!)!
        let second = Int(secondNumberTextField.text!)!
        
        var disposable: Disposable?
        disposable = self.findPrimesEratosthenesRX(first, second)
            .observeOn(MainScheduler.instance)
            .subscribe { e in
            switch e {
            case .next(let tuple):
                print("Received: \(tuple)")
                self.resultLabel.text = "\(self.resultLabel.text ?? "") \(tuple.0)"
                self.countLabel.text = "Count = \(tuple.1)"
                break
            case .error(let error):
                print("Error \(error)")
                self.resultLabel.text = "There are no prime numbers in this range"
                break
            case .completed:
                break
            }
        }
 
    }
    
    @objc func checkStartButton(_ textField: UITextField) {
        if (textField.text?.isEmpty)! {
                disableButton(startButton)
                return
        }
        guard
            let first = firstNumberTextField.text, !first.isEmpty,
            let second = secondNumberTextField.text, !second.isEmpty
            else {
                disableButton(startButton)
                return
        }
        enableButton(startButton)
    }
    
    func disableButton(_ button: UIButton) {
        button.isUserInteractionEnabled = false
        button.alpha = 0.5
    }
    
    func enableButton(_ button: UIButton) {
        button.isUserInteractionEnabled = true
        button.alpha = 1.0
    }
    
}

